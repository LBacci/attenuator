﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.DefaultMenu" Type="Str">dir.mnu</Property>
	<Property Name="NI.Lib.Description" Type="Str">Attenuator Control Module ACM V1 COHERENT Instrument Driver</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)P!!!*Q(C=\:4&gt;L2J"$)80D&lt;*38OEAIA7XQ!-.U)*&lt;Y05_UI*&lt;I!6;/#X1!CW1&lt;W@.DS)F3&amp;'C*.+&gt;7=.Q\,'`H2F''OW,^&amp;H86UU`&lt;4`W4^`ZN^PZ?4C8Z_\@$P^4Q$1SX0S,[TFAGDI`IQH(^0R^;TX`-?]_?MT`R@@\](`Y`Q@`[\`X_]OA0^SENU'+'3MMM2BP&gt;T7[U9VO&gt;+-&lt;P&gt;!,P&gt;!,P&gt;!,0&gt;%40&gt;%40&gt;%40&gt;!$0&gt;!$0&gt;!$@82UI1N&gt;[+S++7Y+G;1GA1E'2?&lt;$]"A?QW.Y_#H$9XA-D_%R0)4)]"A?QW.Y$!`4:(A-D_%R0);(6)0%I[0$9XB)LY+HY#FY#J[#BZ)K?!K!IFC2O%A#BAJH-3BY#J[#B[%+HI+HY#FY#B\=+HA+HI+HY#FYG$*7J1&lt;.X.(B)9U3HI1HY5FY%BZ3+_&amp;*?"+?B#@BI:Q3HI1HA5A+*MF"5$)J#5B_*$Q*$V^+?"+?B#@B38BQD2X+M4)TT&gt;T2Y1FY!J[!*_!*?%CBA#@A#8A#HI#(N!JY!J[!*_!*?#CFA#@A#8A#E+!IZ25E#S9'19%1]0#-UR*DFWK1R/D^VVQ/KPI!KA_7_M#I$Y*[A^5&lt;J^Y1^5+L&amp;V#^-/I86L_)'F"&gt;7*V1(;A,HW@MB"WR!\&lt;(&gt;NA'7W/L??JP$LR=,DK@TTK&gt;4DI?DTI=$NLP^^LN&gt;NJM.FKPVVKN6P@&lt;[CN^;7_X?_G&gt;]&gt;_QZ6\[&amp;_Z'@&gt;,V[:JHD&lt;Y"A&gt;%!!A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
	</Item>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Action-Status" Type="Folder">
			<Item Name="Action-Status.mnu" Type="Document" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Action-Status/Action-Status.mnu"/>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="Configure.mnu" Type="Document" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Configure/Configure.mnu"/>
			<Item Name="SetMODE.vi" Type="VI" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Configure/SetMODE.vi"/>
			<Item Name="SetTRANSMISSION.vi" Type="VI" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Configure/SetTRANSMISSION.vi"/>
			<Item Name="SetPOSITION.vi" Type="VI" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Configure/SetPOSITION.vi"/>
			<Item Name="SetREFENERGY.vi" Type="VI" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Configure/SetREFENERGY.vi"/>
			<Item Name="SetAVERAGE.vi" Type="VI" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Configure/SetAVERAGE.vi"/>
			<Item Name="SetCYCLE.vi" Type="VI" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Configure/SetCYCLE.vi"/>
		</Item>
		<Item Name="Data" Type="Folder">
			<Item Name="Data.mnu" Type="Document" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Data/Data.mnu"/>
			<Item Name="GetENERGY.vi" Type="VI" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Data/GetENERGY.vi"/>
			<Item Name="GetLASTnENERGY.vi" Type="VI" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Data/GetLASTnENERGY.vi"/>
			<Item Name="GetLASTnFENERGY.vi" Type="VI" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Data/GetLASTnFENERGY.vi"/>
			<Item Name="GetLASTnTRANSMISSION.vi" Type="VI" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Data/GetLASTnTRANSMISSION.vi"/>
			<Item Name="GetMODE.vi" Type="VI" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Data/GetMODE.vi"/>
			<Item Name="GetTRANSMISSION.vi" Type="VI" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Data/GetTRANSMISSION.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="Utility.mnu" Type="Document" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Utility/Utility.mnu"/>
			<Item Name="Status Query.vi" Type="VI" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Utility/Status Query.vi"/>
		</Item>
		<Item Name="dir.mnu" Type="Document" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/dir.mnu"/>
		<Item Name="Close.vi" Type="VI" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Close.vi"/>
		<Item Name="Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/Initialize.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Public/VI Tree.vi"/>
	</Item>
	<Item Name="Attenuator Module ACM V1 Readme.html" Type="Document" URL="/&lt;instrlib&gt;/Attenuator Module ACM V1/Attenuator Module ACM V1 Readme.html"/>
</Library>
